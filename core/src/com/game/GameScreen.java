package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dmitriy on 22.02.2016.
 */
public class GameScreen implements Screen {
    static public Sprite plane;
    static public Sprite life;
    public int score = 0;
    private MyGame myGame;
    private Sprite bullet;
    private MyInputController myInputController;
    private List<BulletPlane> bulletsPlane;
    private List<BulletBot> bulletsBot;
    private List<Bot> bots;
    private List<Planet> planets;
    private List<Health> healths;
    private int pauseBulletPlane = 0;
    private int pauseBulletBot = 0;
    private int pauseBot = 0;
    private int pausePlanet = 0;
    private int pauseHealth = 0;
    private Sprite health;
    private Sprite bot1;
    private Sprite bot2;
    private Sprite bot3;
    private Sprite bot4;
    private Sprite star1;
    private Sprite star2;
    private Sprite star3;
    private Sprite star4;
    private Sprite star5;
    private Sprite star6;
    private Sprite star7;
    private Sprite life0;
    private Sprite life12;
    private Sprite life24;
    private Sprite life36;
    private Sprite life48;
    private Sprite life60;
    private Random random;
    private int randNumberBot = 30;
    private int randNumberPlanet = 240;
    private int missed = 0;
    private boolean side = false;
    private int hp;
    private List<Explosion> animations;

    public GameScreen(MyGame myGame) {
        this.myGame = myGame;
        plane = new Sprite(new Texture("bots/plane.png"));
        health = new Sprite(new Texture("health.png"));
        bot1 = new Sprite(new Texture("bots/bot1.png"));
        bot2 = new Sprite(new Texture("bots/bot2.png"));
        bot3 = new Sprite(new Texture("bots/bot3.png"));
        bot4 = new Sprite(new Texture("bots/bot4.png"));
        star1 = new Sprite(new Texture("planets/star1.png"));
        star2 = new Sprite(new Texture("planets/star2.png"));
        star3 = new Sprite(new Texture("planets/star3.png"));
        star4 = new Sprite(new Texture("planets/star4.png"));
        star5 = new Sprite(new Texture("planets/star5.png"));
        star6 = new Sprite(new Texture("planets/star6.png"));
        star7 = new Sprite(new Texture("planets/star7.png"));
        life0 = new Sprite(new Texture("life/life0.png"));
        life12 = new Sprite(new Texture("life/life12.png"));
        life24 = new Sprite(new Texture("life/life24.png"));
        life36 = new Sprite(new Texture("life/life36.png"));
        life48 = new Sprite(new Texture("life/life48.png"));
        life60 = new Sprite(new Texture("life/life60.png"));
        life = life60;
        hp = 60;
        plane.setX(0);
        plane.setY(Gdx.graphics.getHeight() / 2);
        life.setX(16);
        life.setY(Gdx.graphics.getHeight() / 2 + plane.getHeight() / 2 + 10);
        myInputController = new MyInputController();
        bullet = new Sprite(new Texture("bullet.png"));
        bulletsPlane = new LinkedList<BulletPlane>();
        bulletsBot = new LinkedList<BulletBot>();
        bots = new LinkedList<Bot>();
        planets = new LinkedList<Planet>();
        healths = new LinkedList<Health>();
        Gdx.input.setInputProcessor(myInputController);
        random = new Random();
        animations = new LinkedList<Explosion>();
    }

    @Override
    public void show() {
        Gdx.input.setCatchBackKey(true);//отлов кнопки назад на андроид и её блокировка
        myGame.musicMenu.pause();
        myGame.musicGame.play();
    }

    @Override
    public void render(float delta) {
        pauseHealth++;
        pauseBulletPlane++;
        pauseBulletBot++;
        pauseBot++;
        pausePlanet++;
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        myGame.batch.begin();
        if (hp <= 36 && pauseHealth > 180) {
            pauseHealth = 0;
            healths.add(new Health(Gdx.graphics.getWidth(), random.nextInt(Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - health.getHeight()))))));
        }
        if (pauseBulletPlane == 35) {
            pauseBulletPlane = 0;
            if (side) {
                bulletsPlane.add(new BulletPlane(45, plane.getY() + plane.getHeight() / 2 + 14));
                side = false;
            } else {
                bulletsPlane.add(new BulletPlane(45, plane.getY() + plane.getHeight() / 2 - 17));
                side = true;
            }
        }
        if (pauseBot >= randNumberBot) {
            randNumberBot = Math.abs(random.nextInt(30)) + 30;//через рандомное время появление ботов
            pauseBot = 0;
            switch (Math.abs(random.nextInt(4))) {
                case 0: {
                    bots.add(new Bot(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - bot1.getHeight()))), bot1.getHeight(), bot1.getWidth(), 1, Math.abs(random.nextInt(3)) + 4));
                    break;
                }
                case 1: {
                    bots.add(new Bot(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - bot2.getHeight()))), bot2.getHeight(), bot2.getWidth(), 2, Math.abs(random.nextInt(3)) + 4));
                    break;
                }
                case 2: {
                    bots.add(new Bot(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - bot3.getHeight()))), bot3.getHeight(), bot3.getWidth(), 3, Math.abs(random.nextInt(3)) + 4));
                    break;
                }
                case 3: {
                    bots.add(new Bot(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - bot4.getHeight()))), bot4.getHeight(), bot4.getWidth(), 4, Math.abs(random.nextInt(3)) + 4));
                    break;
                }
            }
        }
        if (pauseBulletBot >= 40 && bots.size() > 0) {
            pauseBulletBot = 0;
            Bot bot = bots.get(Math.abs(random.nextInt(bots.size())));
            bulletsBot.add(new BulletBot(bot.x, bot.y + bot.height / 2, bot));
        }
        if (pausePlanet >= randNumberPlanet) {
            randNumberPlanet = Math.abs(random.nextInt(120)) + 240;//через рандомное время появление планет
            pausePlanet = 0;
            switch (Math.abs(random.nextInt(7))) {
                case 0: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star1.getHeight()))), star1.getHeight(), star1.getWidth(), 1, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 1: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star2.getHeight()))), star2.getHeight(), star2.getWidth(), 2, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 2: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star3.getHeight()))), star3.getHeight(), star3.getWidth(), 3, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 3: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star4.getHeight()))), star4.getHeight(), star4.getWidth(), 4, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 4: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star5.getHeight()))), star5.getHeight(), star5.getWidth(), 5, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 5: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star6.getHeight()))), star6.getHeight(), star6.getWidth(), 6, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 6: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star7.getHeight()))), star7.getHeight(), star7.getWidth(), 7, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
            }
        }
        for (int k = 0; k < planets.size(); k++) {
            if (planets.get(k).x + planets.get(k).width > 0) {
                switch (planets.get(k).numberOfPlanet) {
                    case 1: {
                        star1.setX(planets.get(k).x);
                        star1.setY(planets.get(k).y);
                        star1.draw(myGame.batch);
                        break;
                    }
                    case 2: {
                        star2.setX(planets.get(k).x);
                        star2.setY(planets.get(k).y);
                        star2.draw(myGame.batch);
                        break;
                    }
                    case 3: {
                        star3.setX(planets.get(k).x);
                        star3.setY(planets.get(k).y);
                        star3.draw(myGame.batch);
                        break;
                    }
                    case 4: {
                        star4.setX(planets.get(k).x);
                        star4.setY(planets.get(k).y);
                        star4.draw(myGame.batch);
                        break;
                    }
                    case 5: {
                        star5.setX(planets.get(k).x);
                        star5.setY(planets.get(k).y);
                        star5.draw(myGame.batch);
                        break;
                    }
                    case 6: {
                        star6.setX(planets.get(k).x);
                        star6.setY(planets.get(k).y);
                        star6.draw(myGame.batch);
                        break;
                    }
                    case 7: {
                        star7.setX(planets.get(k).x);
                        star7.setY(planets.get(k).y);
                        star7.draw(myGame.batch);
                        break;
                    }
                }

            } else {
                planets.remove(k);
                continue;//??break??
            }
            planets.set(k, new Planet(planets.get(k).x - planets.get(k).speedPlanet, planets.get(k).y, planets.get(k).height, planets.get(k).width, planets.get(k).numberOfPlanet, planets.get(k).speedPlanet));
        }
        for (int i = 0; i < animations.size(); i++) {
            animations.get(i).setStateTime(Gdx.graphics.getDeltaTime());
            myGame.batch.draw(animations.get(i).getExplosionRegion(), animations.get(i).getX(), animations.get(i).getY());
            if (animations.get(i).getAnimation().isAnimationFinished(animations.get(i).getStateTime())) {
                animations.remove(i);
            }
        }
        plane.draw(myGame.batch);
        for (int i = 0; i < bulletsBot.size(); i++) {
            if (bulletsBot.get(i).x > 0) {
                if (bulletsBot.get(i).x <= plane.getX() + plane.getWidth() && bulletsBot.get(i).y >= plane.getY() &&
                        bulletsBot.get(i).y <= plane.getY() + plane.getHeight()) {
                    bulletsBot.remove(i);
                    hp -= 12;
                    if (hp == 0) {
                        //animations.add(new Explosion(plane.getX(), plane.getY()));
                        myGame.setScreen(myGame.gameOverScreen);
                        dispose();
                    }
                    myGame.explosion.play();
                } else {
                    bullet.setX(bulletsBot.get(i).x);
                    bullet.setY(bulletsBot.get(i).y);
                    bullet.draw(myGame.batch);
                    bulletsBot.set(i, new BulletBot(bulletsBot.get(i).x - 3 - bulletsBot.get(i).bot.speedBot, bulletsBot.get(i).y, bulletsBot.get(i).bot));
                }
            } else {
                bulletsBot.remove(i);
            }
        }
        switch (hp) {
            case 0: {
                life = life0;
                break;
            }
            case 12: {
                life = life12;
                break;
            }
            case 24: {
                life = life24;
                break;
            }
            case 36: {
                life = life36;
                break;
            }
            case 48: {
                life = life48;
                break;
            }
            case 60: {
                life = life60;
                break;
            }
        }
        life.setX(16);
        life.setY(plane.getY() + plane.getHeight() / 2 + 10);
        life.draw(myGame.batch);
        label:
        for (int i = 0; i < bulletsPlane.size(); i++) {
            if (bulletsPlane.get(i).x < Gdx.graphics.getWidth()) {
                for (int j = 0; j < bots.size(); j++) {
                    if (bulletsPlane.get(i).x + bullet.getWidth() >= bots.get(j).x && bulletsPlane.get(i).y >= bots.get(j).y &&
                            bulletsPlane.get(i).y <= bots.get(j).y + bots.get(j).height) {
                        bulletsPlane.remove(i);
                        animations.add(new Explosion(bots.get(j).x, bots.get(j).y));
                        bots.remove(j);
                        score++;
                        myGame.explosion.play();
                        break label;//просто break(если пули летят одновременно,иначе можно пропустить какие-нибудь столкновения)
                    }
                }
                bullet.setX(bulletsPlane.get(i).x);
                bullet.setY(bulletsPlane.get(i).y);
                bullet.draw(myGame.batch);
            } else {
                bulletsPlane.remove(i);
                continue;//? Не break label; ?
            }
            bulletsPlane.set(i, new BulletPlane(bulletsPlane.get(i).x + 5, bulletsPlane.get(i).y));
        }
        for (int j = 0; j < bots.size(); j++) {
            if (bots.get(j).x + bots.get(j).width > 0) {
                switch (bots.get(j).numberOfBot) {
                    case 1: {
                        bot1.setX(bots.get(j).x);
                        bot1.setY(bots.get(j).y);
                        bot1.draw(myGame.batch);
                        break;
                    }
                    case 2: {
                        bot2.setX(bots.get(j).x);
                        bot2.setY(bots.get(j).y);
                        bot2.draw(myGame.batch);
                        break;
                    }
                    case 3: {
                        bot3.setX(bots.get(j).x);
                        bot3.setY(bots.get(j).y);
                        bot3.draw(myGame.batch);
                        break;
                    }
                    case 4: {
                        bot4.setX(bots.get(j).x);
                        bot4.setY(bots.get(j).y);
                        bot4.draw(myGame.batch);
                        break;
                    }
                }

            } else {
                bots.remove(j);
                missed++;
                continue;//??break??
            }
            bots.set(j, new Bot(bots.get(j).x - bots.get(j).speedBot, bots.get(j).y, bots.get(j).height, bots.get(j).width, bots.get(j).numberOfBot, bots.get(j).speedBot));
        }
        for (int i = 0; i < healths.size(); i++) {
            if (healths.get(i).x > 0) {
                if (healths.get(i).x <= plane.getX() + plane.getWidth() && healths.get(i).y >= plane.getY() &&
                        healths.get(i).y + health.getHeight() <= plane.getY() + plane.getHeight()) {
                    hp = 60;
                    healths.remove(i);
                } else {
                    health.setX(healths.get(i).x);
                    health.setY(healths.get(i).y);
                    health.draw(myGame.batch);
                    healths.set(i, new Health(healths.get(i).x - 5, healths.get(i).y));
                }
            } else {
                healths.remove(i);
            }
        }
        myGame.fontGame.draw(myGame.batch, "score: " + score + ", missed: " + missed, Gdx.graphics.getWidth() / 2 - 70, Gdx.graphics.getHeight() - 10);
        myGame.batch.end();
        if (Gdx.input.isKeyPressed(Input.Keys.BACK)) {
            myGame.setScreen(myGame.menuScreen);
            pause();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
    }

    public class BulletPlane {
        private float x;
        private float y;

        public BulletPlane(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }

    public class BulletBot {
        private float x;
        private float y;
        private Bot bot;

        public BulletBot(float x, float y, Bot bot) {
            this.x = x;
            this.y = y;
            this.bot = bot;
        }
    }

    public class Bot {
        private float x;
        private float y;
        private float height;
        private float width;
        private int numberOfBot;
        private int speedBot;

        public Bot(float x, float y, float height, float width, int numberOfBot, int speedBot) {
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.numberOfBot = numberOfBot;
            this.speedBot = speedBot;
        }
    }

    public class Planet {
        private float x;
        private float y;
        private float height;
        private float width;
        private int numberOfPlanet;
        private int speedPlanet;

        public Planet(float x, float y, float height, float width, int numberOfBot, int speedBot) {
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.numberOfPlanet = numberOfBot;
            this.speedPlanet = speedBot;
        }
    }

    public class Health {
        private float x;
        private float y;

        public Health(float x, float y) {
            this.x = x;
            this.y = y;
        }
    }
}
