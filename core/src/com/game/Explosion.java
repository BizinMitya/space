package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Dmitriy on 26.02.2016.
 */
public class Explosion {
    private static final int FRAME_COLS = 5;
    private static final int FRAME_ROWS = 1;
    private Texture explosion;
    private TextureRegion[] explosions;
    private Animation animation;
    private TextureRegion explosionRegion;
    private float stateTime;
    private float x;
    private float y;

    public Explosion(float x, float y) {
        explosion = new Texture(Gdx.files.internal("explosion.png"));
        TextureRegion[][] tmp = TextureRegion.split(explosion, explosion.getWidth() / FRAME_COLS, explosion.getHeight() / FRAME_ROWS);
        explosions = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int index = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                explosions[index++] = tmp[i][j];
            }
        }
        animation = new Animation(0.1f, explosions);
        setStateTime(0f);
        this.x = x;
        this.y = y;
    }

    public TextureRegion getExplosionRegion() {
        return explosionRegion = getAnimation().getKeyFrame(getStateTime());
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getStateTime() {
        return stateTime;
    }

    public void setStateTime(float stateTime) {
        this.stateTime += stateTime;
    }

    public Animation getAnimation() {
        return animation;
    }
}
