package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;

/**
 * Created by Dmitriy on 19.02.2016.
 */
public class MyInputController implements InputProcessor {
    @Override
    public boolean keyDown(int keycode) {
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        //MyGame.plane.setX(MyGame.plane.getX() + Gdx.input.getDeltaX(pointer));
        if (GameScreen.plane.getY() - Gdx.input.getDeltaY(pointer) > 0 && GameScreen.plane.getY() - Gdx.input.getDeltaY(pointer) + GameScreen.plane.getHeight() < Gdx.graphics.getHeight()) {
            GameScreen.plane.setY(GameScreen.plane.getY() - Gdx.input.getDeltaY(pointer));
            GameScreen.life.setY(GameScreen.life.getY() - Gdx.input.getDeltaY(pointer));
        }
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
