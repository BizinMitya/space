package com.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

public class MyGame extends Game {
    public SpriteBatch batch;
    public BitmapFont fontMenu;
    public BitmapFont fontGame;
    public FreeTypeFontGenerator generator;
    public FreeTypeFontGenerator.FreeTypeFontParameter parameter;
    public Sound explosion;
    public MenuScreen menuScreen;
    public GameScreen gameScreen;
    public GameOverScreen gameOverScreen;
    public Music musicGame;
    public Music musicMenu;

    @Override
    public void create() {
        batch = new SpriteBatch();
        generator = new FreeTypeFontGenerator(Gdx.files.internal("arial.ttf"));
        parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 60;
        fontMenu = generator.generateFont(parameter);
        parameter.size = 20;
        fontGame = generator.generateFont(parameter);
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.mp3"));
        explosion.setVolume(explosion.play(), 0.2f);
        menuScreen = new MenuScreen(this);
        gameScreen = new GameScreen(this);
        gameOverScreen = new GameOverScreen(this);
        musicGame = Gdx.audio.newMusic(Gdx.files.internal("data/musicGame.mp3"));
        musicMenu = Gdx.audio.newMusic(Gdx.files.internal("data/musicMenu.mp3"));
        musicGame.setVolume(1f);
        musicMenu.setVolume(1f);
        //musicGame.play();
        musicGame.setLooping(true);
        musicMenu.setLooping(true);
        this.setScreen(menuScreen);
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        super.dispose();
        batch.dispose();
        fontGame.dispose();
        fontMenu.dispose();
        generator.dispose();
        explosion.dispose();
        musicGame.dispose();
        musicMenu.dispose();
    }
}
