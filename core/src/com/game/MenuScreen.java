package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by Dmitriy on 21.02.2016.
 */
public class MenuScreen implements Screen {
    private MyGame myGame;
    private List<Planet> planets;
    private int pausePlanet = 0;
    private Random random;
    private Sprite star1;
    private Sprite star2;
    private Sprite star3;
    private Sprite star4;
    private Sprite star5;
    private Sprite star6;
    private Sprite star7;
    private int randNumberPlanet = 240;

    public MenuScreen(MyGame myGame) {
        this.myGame = myGame;
        planets = new LinkedList<Planet>();
        random = new Random();
        star1 = new Sprite(new Texture("planets/star1.png"));
        star2 = new Sprite(new Texture("planets/star2.png"));
        star3 = new Sprite(new Texture("planets/star3.png"));
        star4 = new Sprite(new Texture("planets/star4.png"));
        star5 = new Sprite(new Texture("planets/star5.png"));
        star6 = new Sprite(new Texture("planets/star6.png"));
        star7 = new Sprite(new Texture("planets/star7.png"));
    }


    @Override
    public void show() {
        Gdx.input.setCatchBackKey(false);
        myGame.musicGame.pause();
        myGame.musicMenu.play();
    }

    @Override
    public void render(float delta) {
        pausePlanet++;
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        myGame.batch.begin();
        if (pausePlanet == randNumberPlanet) {
            randNumberPlanet = Math.abs(random.nextInt(120)) + 240;//через рандомное время появление планет
            pausePlanet = 0;
            switch (Math.abs(random.nextInt(7))) {
                case 0: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star1.getHeight()))), star1.getHeight(), star1.getWidth(), 1, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 1: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star2.getHeight()))), star2.getHeight(), star2.getWidth(), 2, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 2: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star3.getHeight()))), star3.getHeight(), star3.getWidth(), 3, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 3: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star4.getHeight()))), star4.getHeight(), star4.getWidth(), 4, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 4: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star5.getHeight()))), star5.getHeight(), star5.getWidth(), 5, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 5: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star6.getHeight()))), star6.getHeight(), star6.getWidth(), 6, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
                case 6: {
                    planets.add(new Planet(Gdx.graphics.getWidth(), Math.abs(random.nextInt((int) (Gdx.graphics.getHeight() - star7.getHeight()))), star7.getHeight(), star7.getWidth(), 7, Math.abs(random.nextInt(2)) + 1));
                    break;
                }
            }
        }
        for (int k = 0; k < planets.size(); k++) {
            if (planets.get(k).x + planets.get(k).width > 0) {
                switch (planets.get(k).numberOfPlanet) {
                    case 1: {
                        star1.setX(planets.get(k).x);
                        star1.setY(planets.get(k).y);
                        star1.draw(myGame.batch);
                        break;
                    }
                    case 2: {
                        star2.setX(planets.get(k).x);
                        star2.setY(planets.get(k).y);
                        star2.draw(myGame.batch);
                        break;
                    }
                    case 3: {
                        star3.setX(planets.get(k).x);
                        star3.setY(planets.get(k).y);
                        star3.draw(myGame.batch);
                        break;
                    }
                    case 4: {
                        star4.setX(planets.get(k).x);
                        star4.setY(planets.get(k).y);
                        star4.draw(myGame.batch);
                        break;
                    }
                    case 5: {
                        star5.setX(planets.get(k).x);
                        star5.setY(planets.get(k).y);
                        star5.draw(myGame.batch);
                        break;
                    }
                    case 6: {
                        star6.setX(planets.get(k).x);
                        star6.setY(planets.get(k).y);
                        star6.draw(myGame.batch);
                        break;
                    }
                    case 7: {
                        star7.setX(planets.get(k).x);
                        star7.setY(planets.get(k).y);
                        star7.draw(myGame.batch);
                        break;
                    }
                }

            } else {
                planets.remove(k);
                continue;//break??
            }
            planets.set(k, new Planet(planets.get(k).x - planets.get(k).speedPlanet, planets.get(k).y, planets.get(k).height, planets.get(k).width, planets.get(k).numberOfPlanet, planets.get(k).speedPlanet));
        }
        myGame.fontMenu.draw(myGame.batch, "Tap anywhere to continue", 120, Gdx.graphics.getHeight() - 250);
        myGame.batch.end();
        if (Gdx.input.isTouched()) {
            myGame.setScreen(myGame.gameScreen);
            pause();
        }
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public class Planet {
        private float x;
        private float y;
        private float height;
        private float width;
        private int numberOfPlanet;
        private int speedPlanet;

        public Planet(float x, float y, float height, float width, int numberOfBot, int speedBot) {
            this.x = x;
            this.y = y;
            this.height = height;
            this.width = width;
            this.numberOfPlanet = numberOfBot;
            this.speedPlanet = speedBot;
        }
    }
}
